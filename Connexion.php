<?php
session_start();


$login = (isset($_POST['login'])?$_POST['login']:null);
$password = (isset($_POST['password'])?$_POST['password']:null);

$logs = [$login => $password];

if (isset($_POST['Reset'])){
    $login = null;
    $password = null;
}


function existeUtilisateurbdd (array $logins){
    include "tobdd.php";
    foreach ($logins as $login => $password) {
        $req = "SELECT * FROM utilisateur WHERE login = :login";
        $select = $db->prepare($req);
        $select -> execute(array(':login' => $login));
        $resultat = $select->fetch();

        if ($resultat){
            if (password_verify($password, $resultat['mdp'])){
                $_SESSION['id'] = $resultat['idutilisateur'];
                $_SESSION['mdp'] = $password;
                if ($resultat['admin'] == 1) {
                    $_SESSION['admin'] = true;
                }
                else {
                    $_SESSION['admin'] = false;
                }
                return true;
            } else {
                echo "Mot de passe incorrect";
                return false;
            }
        } else {
            echo "Utilisateur inconnu";
            return false;
        }
    }
}




include 'Connexion.view.php';
?>
