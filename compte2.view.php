<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <title>
        <?= $titre ?>
    </title>
    <link rel="stylesheet" href="cmp.css" />
</head>
<body>
    <section class="box2">
        <article>
            <h3>
                <?= $titre ?>
            </h3>
            <form method="POST" action="" name="add">
                <table class="formulaire">
                    <tr>
                        <td><label for="nom">Nom : </label></td>
                        <td><input type="text" name="nom" id="nom" size="40" value="<?= $valeurs['nom'] ?>" disabled /></td>
                    </tr>
                    <tr>
                        <td><label for="prn">Prénom: </label></td>
                        <td><input type="text" name="prn" id="prn" size="40" value="<?= $valeurs['prenom'] ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><span class="erreur">
                                <?php echo $erreurs['prenom']; ?>
                            </span></td>
                    </tr>
                    <tr>
                        <td><label for="mel">Email : </label></td>
                        <td><input type="text" name="mel" id="mel" value="<?= $valeurs['mel'] ?>" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><span class="erreur">
                                <?php echo $erreurs['mel']; ?>
                            </span></td>
                    </tr>
                    <tr>
                        <td><label for="civi">Civilité : </label></td>
                        <td><input type="radio" name="civilite"  value = "Mme" <?php echo ($valeurs['civil']  == "Mme")?'checked = "checked"': "" ?>/>Féminin 
                        <input type="radio" name="civilite"  value = "M." <?php  if ($valeurs['civil'] === "M." || $valeurs['civil'] ===null) echo 'checked = "checked;"'?> />Masculin</td>
                    </tr>
                    <tr>
                        <td><label for="login">Login : </label></td>
                        <td><input type="text" name="login" id="login" value="<?= $valeurs['login'] ?>" disabled /></td>
                    </tr>
                    <?php 
                    if($_SESSION['edit']!==null){
                    ?>
                        <tr>
                        <td><label for="status">Status : </label></td>
                        <td><input type="radio" name="admin"  value = "1" <?php echo ($valeurs['admin'] == 1)? 'checked = "checked"': "" ?>/>Admin
                        <input type="radio" name="admin"  value = "0" <?php  if ($valeurs['admin'] == 0) echo  'checked = "checked;"' ?> />Client</td>
                        </tr>
                    <?php 
                        }else{
                    ?>
                     <tr>
                        <td><label for="mdp">Mot de passe : </label></td>
                        <td><input type="text" name="mdp" id="mdp" value="<?= $valeurs['mdp'] ?>" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><span class="erreur">
                                <?php echo $erreurs['mdp']; ?>
                            </span></td>
                    </tr>
                    <?php 
                        }
                    ?>
                </table>
                <div class="boutonmodif paddtop">
                    <input type="submit" id=<?= $bouton; ?> name=<?= $bouton; ?>
                        value=<?= $bouton; ?> />
                    <input type="submit" id="annuler" name="Annuler" value="Annuler" />
                </div>
            </form>
        </article>
    </section>
</body>

</html>



