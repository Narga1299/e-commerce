<?php
require_once('tobdd.php');
require_once('compte.php');
require_once('Requete.php');

$valeurs['nom'] = (isset($_POST['Nom']) ? $_POST['Nom'] : null);
$valeurs['prenom'] = (isset($_POST['Prenom']) ? $_POST['Prenom'] : null);
$valeurs['mel'] = (isset($_POST['Mail']) ? $_POST['Mail'] : null);
$valeurs['civil'] = (isset($_POST['Civilite']) ? $_POST['Civilite'] : null);
$valeurs['login'] = (isset($_POST['Logi']) ? $_POST['Logi'] : null);
if ($_SESSION['edit'] !== null) {
    $valeurs['admin'] = (isset($_POST['admin']) ? $_POST['admin'] : null);
}
else{
    $valeurs['mdp'] = $_SESSION['mdp'];
}

if ($_SESSION['edit'] !== null) {
    $erreurs = ['mel' => "", 'prenom' => ""];
}
else{
    $erreurs = ['mel' => "", 'prenom' => "", 'mdp' => ""];
}

$titre = "Modification du compte";
$bouton = "Modifier";

$iduti = $_SESSION['id'];
if ($_SESSION['edit']!==null){
$iduti = $_SESSION['edit'];
}
$reqUnUti = "SELECT * FROM utilisateur WHERE idutilisateur = $iduti";
$resultat = $db->query($reqUnUti);
$resulta = $resultat->fetch();

$valeurs['nom'] = $resulta['nom'];
$valeurs['prenom'] = $resulta['prenom'];
$valeurs['mel'] = $resulta['mel'];
$valeurs['civil'] = $resulta['civilite'];
$valeurs['login'] = $resulta['login'];
if ($_SESSION['edit']!==null){
    $valeurs['admin'] = $resulta['admin'];
}

unset($resulta);


if (isset($_POST['Annuler']))
    header('location: compte.php');

if ((isset($_POST['Modifier']))) {
   
    $erreurs = erreursDonnees($valeurs,$_SESSION['edit']);
    if (donneesOK($erreurs)) {

        $modif['prenom'] = (isset($_POST['prn']) ? ($_POST['prn']) : null);
        $modif['mel'] = (isset($_POST['mel']) ? $_POST['mel'] : null);
        $modif['civilite'] = (isset($_POST['civilite']) ? $_POST['civilite'] : null);
        if ($_SESSION['edit']!==null){
            $modif['admin'] = (isset($_POST['admin']) ? $_POST['admin'] : null);
        }
        else{
            $modif['mdp'] = (isset($_POST['mdp']) ? $_POST['mdp'] : null);
        }


        $erreurs = erreursDonnees($modif,$_SESSION['edit']);

        if (donneesOK($erreurs)) {
            if ($_SESSION['edit']!==null){
                $result = $db->prepare($reqUpdateUtiAdmin);
                $result->execute([
                    ':prenom' => $modif['prenom'],
                    ':mel' => $modif['mel'],
                    ':civilite' => $modif['civilite'],
                    ':admin' => $modif['admin'],
                    ':id' => $iduti
                ]);
            }
            else{
            $mdphash = password_hash($modif['mdp'], PASSWORD_DEFAULT); 
            $result = $db->prepare($reqUpdateUti);
            $result->execute(
                [
                    ':id' => $iduti,
                    ':prenom' => $modif['prenom'],
                    ':mel' => $modif['mel'],
                    ':civilite' => $modif['civilite'],
                    ':mdp' => $mdphash
                ]
            );
            }
            $_SESSION['mdp'] = $modif['mdp'];
            unset($result);
            header('location: ./compte.php');
        }
    }
}

include 'compte2.view.php';
?>


