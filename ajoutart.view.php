<!doctype html>
 <html>
<head>
        <meta charset='SET NAMES utf8'/>
        <link rel="stylesheet" href="ajoutart.css">
    <title>Page admin</title>
</head>
<header class="fnoir">
        <h1>Boutique en ligne de l'IUT de Metz</h1>
</header>
<body>
    <h2>Ajout d'article</h2>
    <div class="box">
    <form  method = 'post' action = 'ajoutart.php' name='form1'>
        <div>
            <h3>Saisie des informations de l'article</h3>
            <div>
                <label>Reference :</label>
                <input type="text" name="refart" value = "<?=$refart?>" placeholder="Indiquez la Reference" />
            </div>
            <span class="erreur"><?php echo $erreurs['refart']; ?></span><br>
            <div>
                <label>Designation :</label>
                <input type="text" name="designation" value = '<?=$designation?>' placeholder="Indiquez la designation"/>
            </div>
            <span class="erreur"><?php echo $erreurs['designation']; ?></span><br>
            <div>
                <label>Prix unite :</label>
                <input type="number" name="pu" value = '<?=$pu?>' placeholder="Indiquez le prix a l'unite"/>
            </div>
            <span class="erreur"><?php echo $erreurs['pu']; ?></span><br>
        </div>
        <div>
            <div>
                <label>Unite de conditionnement :</label>
                <input type="text" name="unitecond" value = '<?=$unitecond?>' placeholder="Indiquez l'unite de conditionnement"/>
            </div>
            <span class="erreur"><?php echo $erreurs['unitecond']; ?></span><br>
        </div>
        <div>
            <div>
                <label>Remise :</label>
                <input type="text" name="remise" value = '<?=$remise?>' placeholder="Indiquez la remise"/>
            </div>
            <span class="erreur"><?php echo $erreurs['remise']; ?></span><br>
        </div>
        <div class="bu">
            <input  type="submit" name="valider" value="Validez">
            <input  type="submit" name="Annuler" value="Annulez">
            <input type="submit" name="retour" value="Retour">
            </form>
        </div>
</body>
</html>