<?php
session_start();
include ('tobdd.php');
include ('Requete.php');


$refart = (isset($_POST['refart'])?$_POST['refart']:null);
$designation = (isset($_POST['designation'])?$_POST['designation']:null);
$pu = (isset($_POST['pu'])?$_POST['pu']:null);
$unitecond = (isset($_POST['unitecond'])?$_POST['unitecond']:null);
$remise = (isset($_POST['remise'])?$_POST['remise']:null);


$erreurs = ['refart'=>"", 'designation'=>"", 'pu'=>"",'unitecond' => "", 'remise' => ""];

function existeArt($refart){
    include ('tobdd.php');
    include ('Requete.php');
    $existe = false;
    $requete = "SELECT * FROM article WHERE refart = '$refart'";
    $resultat = $db->query($requete);
    $compte = $resultat->fetch();
    if ($resultat->rowCount() == 1) {
        $existe = true;
    }
    return $existe;
}

if (isset($_POST['valider'])){
    if (!isset($refart) or empty($refart) or (existeArt($refart))==true){ 
        $erreurs['refart'] = "Veuillez saisir la reference de l'article(unique)";
    }
    if (!isset($designation) or empty($designation)){
        $erreurs['designation'] = "Veuillez saisir la designation de l'article";
    }
    if (!isset($pu) or empty($pu) or ($pu>99.99) or ($pu<0)){
        $erreurs['pu'] = "Veuillez saisir le prix a l'unite de l'article";
    }
    if (!isset($unitecond) or empty($unitecond)){
        $erreurs['unitecond'] = "Veuillez saisir l'unite de conditionnement de l'article";
    }
    if (!isset($remise)){
        $erreurs['remise'] = "Veuillez saisir la remise de l'article";
    }

    $nbErreurs = 0;
    foreach ($erreurs as $erreur){
    if ($erreur != "") $nbErreurs++;
    }

    if ($nbErreurs==0){
    $insert = $db->prepare($ajoutarticle);
    $insert -> execute(array( ':refart' => $refart, 
    ':designation' => $designation, 
    ':pu' => $pu, 
    ':unitecond' => $unitecond, 
    ':remise' => $remise));
    header ('Location: ./admin.php');
}
unset($insert );
 $refart = "";
 $designation = "";
 $pu = "";
 $unitecond = "";
 $remise = "";
}  

if(isset($_POST['retour'])){
    header('Location: ./admin.php');
}

if(isset($_POST['Annuler'])){
    $refart = null;
    $designation = null;
    $pu = null;
    $unitecond = null;
    $remise = null;
    
}




include 'ajoutart.view.php';
?>