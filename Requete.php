<?php

 $listeart = "select * from article";

 $reqUnUti = "SELECT *
 FROM utilisateur
 WHERE idutiliseur = :id";

 $reqInsertUti = "INSERT INTO utilisateur (nom, prenom, civilite,mel,login,mdp)
 VALUES(:nom, :prenom, :civilite,:mel,:login,:mdp)";

 $reqUpdateUti = "UPDATE utilisateur
 SET prenom = :prenom,
    civilite = :civilite,
    mel = :mel,
    mdp = :mdp
 WHERE idutilisateur = :id";

$reqUpdateUtiAdmin = "UPDATE utilisateur
SET prenom = :prenom,
   civilite = :civilite,
   mel = :mel,
   admin = :admin
WHERE idutilisateur = :id";


 $reqDeleteUti = "DELETE FROM utilisateur
 WHERE login = :login";

 $reqAddCadie = "INSERT INTO caddie (idUtilisateur, refart, qte)
 VALUES(:iduti, :refart, :qte)";

$checkcaddie = "SELECT * FROM caddie WHERE idUtilisateur = :iduti AND refart = :refart";
 
$selectQuantite = "SELECT qte FROM caddie WHERE idUtilisateur = :iduti AND refart = :refart";

$udapteCaddie = "UPDATE caddie SET qte = :qte WHERE idUtilisateur = :iduti AND refart = :refart";

$deleteCaddie = "DELETE FROM caddie WHERE idUtilisateur = :iduti";

$deleteArtCaddie = "DELETE FROM caddie WHERE idUtilisateur = :iduti AND refart = :refart";

$totalCaddie ="SELECT round(SUM(pu-(pu*(remise*POW(10,-2))))*qte,2) as total FROM caddie,article WHERE caddie.refart = article.refart AND idUtilisateur = :id";

$affdesi = "SELECT article.designation FROM article,caddie WHERE caddie.refart = article.refart AND idUtilisateur = :iduti";

$deleteArtCaddie = "DELETE FROM caddie WHERE idUtilisateur = :iduti AND refart = :refart";

$listeclient = "SELECT * FROM utilisateur";

$ajoutarticle = "INSERT INTO article (refart, designation, pu, unitecond, remise)
VALUES(:refart, :designation, :pu, :unitecond,:remise)";

$ajoutarticle2 = "INSERT INTO article (refart, designation, pu, unitecond, remise)
VALUES(:refart, :designation, :pu, :unitecond,0)";

$reqUpdateArt = "UPDATE article
 SET refart = :refart,
   designation = :designation,
   pu = :pu ,
   unitecond = :unitecond
 WHERE refart = :refart";

$reqUpdateArt2 = "UPDATE article
SET refart = :refart,
  designation = :designation,
  pu = :pu ,
  unitecond = :unitecond,
   remise = :remise
WHERE refart = :refart";


$supprart = "DELETE FROM article WHERE refart = :refart";
?>
