<!doctype html>
<html>

<head>
    <meta charset="utf8" />
    <link rel="stylesheet" href="magasin2.css">
</head>
<header class="fnoir">
    <h1>Boutique en ligne de l'IUT de Metz</h1>
</header>

<body>
    <h2>Magasin</h2>
    <h3><a href="./compte.php">Compte</a></h3>
    <h4 class="pr">Produits :</h4>
    <div class="art">
        <?php
        foreach ($arts as $art) {
            echo '<form method="POST" action="Magasin.php" name="mag">';
            echo "<table>";
            echo "<tbody>";
            echo "<tr>";
            echo "<td class='td2'>" . $art->getDesignation() . "</td>";
            echo "<td>" . $art->__affichePrix() . "</td>";
        ?>
            <td><input type='number' name='quantite' value='0' size='20' step='any' min='1' max='100'>
                <button type='submit' name='ajouter' value="<?php echo $art->getRefart() ?>"> Ajouter au panier </button>
            </td>
            <?php
            echo "<td>";
            $refart = $art->getRefart();
            echo "<img alt='codebarre' src='./codeBarre.php?reference=$refart'/>";
            echo "</td>";
            ?>
            </tr>
            </tbody>
            </table>
            </form>
        <?php
        }
        ?>
    </div>
    <h4 class="pr">Caddie : </h4></br>
    <div class="caddie">
        <?php
            echo '<form method="POST" action="Magasin.php" name="mag">';
            echo '<div>';
            foreach ($resultat as $caddie) {
            echo "<div class='inline'>";
            echo afficheDesi($arts, $caddie['refart']) . " x" . $caddie['qte'];
            echo "<br>";
            echo "</div>";
        ?>
            <button type='submit' name='supprimer' value="<?php echo $caddie['refart'] ?>"> Supprimer </button>
            <br/>
        <?php
        }
        echo "</br>";
        totalcaddie();
        echo "</div>";
        ?>
        <div class="pad">
            <button type='submit' name="valider" value="<?php echo $iduti ?>"> Valider </button>
            <button type='submit' name='vider' value="<?php echo $iduti ?>"> Vider </button>
        </div>
        <div>
            <span><?php echo $vid ?></span>
        </div>
    </div>
    </br>
    <div class="droit">
        <?php if ($_SESSION['admin'] == 1) {
            echo '<p><a href="./admin.php">Page Admin</a></p>';
        }
        ?>
        <input type="submit" name="deconnexion" size='40px' value="Deconnexion">
    </div>
    </form>
</body>
</html>