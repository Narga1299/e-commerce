<?php
session_start();

include 'tobdd.php';
include 'Requete.php';



$quantite = (isset($_POST['quantite'])?$_POST['quantite']:0);
$iduti = $_SESSION['id'];
$_SESSION['edit'] = null;

class Article
{
    private $refart;
    private $Designation;
    private $Prixunit;
    private $Unitcond;
    private $Remise;




    function __construct($refart, $Designation, $Prixunit, $Unitcond, $Remise)
    {
        $this->refart = $refart;
        $this->Designation = $Designation;
        $this->Prixunit = $Prixunit;
        $this->Unitcond = $Unitcond;
        $this->Remise = $Remise;
    }


    function getRefart()
    {
        return $this->refart;
    }

    function getDesignation()
    {
        return $this->Designation;
    }

    function getPrixunit()
    {
        return $this->Prixunit;
    }

    function getUnitcond()
    {
        return $this->Unitcond;
    }

    function getRemise()
    {
        return $this->Remise;
    }

    function calculPrixRemise()
    {
        return $this->Prixunit - ($this->Prixunit * $this->Remise / 100);
    }

    function __toString()
    {
        return "Référence : " . $this->refart . " Designation : " . $this->Designation;
    }

    function __affichePrix()
    {
        return "Prix :" . $this->Prixunit . "€ <br/> Remise : " . $this->Remise . "<br/> Prix remisé : " . $this->calculPrixRemise() . "€";
    }

}

class caddie {
    private $iduti;
    private $refart;
    private $quantite;

    function __construct($ref){
        $this->iduti = $_SESSION['id']; 
        $this->refart = $ref; 
    }

    function setiduti($iduti){
        $this->iduti = $iduti;
    }

    function setquantite($quantite){
        $this->quantite = $quantite;
    }

    
    function getQuantite(){
        return $this->quantite;
    }

    function insertCaddie(){
        include 'tobdd.php';
        include 'Requete.php';
        $res = $db->prepare($reqAddCadie);
        $res->execute([
            ':iduti' => $this->iduti,
            ':refart' => $this->refart,
            ':qte' => $this->quantite
        ]);
    }

    function existCaddie()
    {
        include 'tobdd.php';
        include 'Requete.php';
        $res = $db->prepare($checkcaddie);
        $res->execute([
            ':iduti' => $this->iduti,
            ':refart' => $this->refart
        ]);
        $res = $res->fetchAll();
        
        if(count($res) > 0){
            return true;
        }else{
            return false;
        }
    }

    function selectQuantiteCaddie()
    {
        include 'tobdd.php';
        include 'Requete.php';
        $res = $db->prepare($selectQuantite);
        $res->execute([
            ':iduti' => $this->iduti,
            ':refart' => $this->refart
        ]);
        $res = $res->fetch();
        return $res;
    }

    public function updateCaddie(){ //verifier que le caddies existe avant de faire l'update
        include 'tobdd.php';
        include 'Requete.php';
        
        $res = $db->prepare($udapteCaddie);
        $res->execute([
            ':iduti' => $this->iduti,
            ':refart' => $this->refart,
            ':qte' => $this->quantite
        ]);
    }
}


function totalcaddie(){
    include 'tobdd.php';
    include 'Requete.php';
    $res = $db->prepare($totalCaddie);
    $res->execute([
        ':id' => $_SESSION['id']
    ]);
    $total = $res->fetch();
    if ($total['total'] == null){
        echo "Total : 0 €";
    }
    else{
        echo "Total : ".$total['total']." €";
    }
}

$arts = [];


$result = $db->prepare($listeart);
$result->execute();

foreach ($result as $art) {
 $arts[] = new Article($art['refart'], $art['designation'], $art['pu'], $art['unitecond'], $art['remise']);
}


if (isset($_POST['ajouter']))
    {
    $caddie= new caddie($_POST['ajouter']);
    $exist = $caddie->existCaddie();
    if($exist){
        $qte = $caddie->selectQuantiteCaddie();
        $qte = $qte = number_format($qte[0], 4);
        $qtefinale = $qte+ $_POST['quantite']; 
        $caddie->setquantite($qtefinale);
        $caddie->updateCaddie();
    }
    else{
        $qte=$_POST['quantite'];
        $caddie->setquantite($qte);
        $caddie->insertCaddie();
    }
    }

$resultat = [];
$req = 'SELECT * from caddie where idUtilisateur = "'.$_SESSION['id'].'" ' ;
$resultat = $db->query($req);


function afficheDesi($arts,$refart){
    foreach($arts as $art){
        if($art->getRefart() == $refart){
            return $art->getDesignation();
        }
    }
}



if (isset($_POST['supprimer']))
{  
    $req = $db->prepare($deleteArtCaddie);
    $req ->execute([
        ':iduti' => $_SESSION['id'],
        ':refart' => $_POST['supprimer'],
    ]);
    header('Location: ./magasin.php');
}
$vid = null;
if (isset($_POST['vider']))
{  
    $vid = "Votre caddie a était vidé";
    $req = $db->prepare($deleteCaddie);
    $req ->execute([
        ':iduti' => $_SESSION['id'],
    ]);
    header('Location: ./Magasin.php');
}

if (isset($_POST['valider']))
{  
    $vid = "Votre caddie a était vidé";
    $req = $db->prepare($deleteCaddie);
    $req ->execute([
        ':iduti' => $_SESSION['id'],
    ]);
    session_destroy();
    header('Location: ./Connexion.php');
}


if (isset($_POST['deconnexion'])) {
    session_destroy();
    header('Location: ./accueil.php');
}


include 'Magasin.view.php';
?>  

