<?php
session_start();

include ('tobdd.php');
include ('Requete.php');


function erreursDonnees(array $donnees)
{
    $erreurs = ['designation' => "", 'pu' => "",'unitecond' => "", 'remise' => ""];

    if (!isset($donnees['designation']) or empty($donnees['designation']))
        $erreurs['designation'] = "saisie obligatoire de la désignation de l'article";
    
    if (!isset($donnees['pu']) or empty($donnees['pu']))
        $erreurs['pu'] = "saisie obligatoire du prix unitaire de l'article";

    if (!isset($donnees['unitecond']) or empty($donnees['unitecond']))
        $erreurs['unitecond'] = "saisie obligatoire de l'unité de conditionnement de l'article";
    
    if (!isset($donnees['remise']) or empty($donnees['remise']))
        $erreurs['remise'] = "saisie obligatoire de la remise de l'article";


    return $erreurs;
}



function donneesOK(array $erreurs)
{
    $nbErreurs = 0;
    foreach ($erreurs as $erreur) {
        if ($erreur != "")
            $nbErreurs++;
    }
    return ($nbErreurs == 0);
}

$valeurs['refart'] = (isset($_POST['refart'])?$_POST['refart']:null);
$valeurs['designation'] = (isset($_POST['designation'])?$_POST['designation']:null);
$valeurs['pu'] = (isset($_POST['pu'])?$_POST['pu']:null);
$valeurs['unitecond']= (isset($_POST['unitecond'])?$_POST['unitecond']:null);
$valeurs['remise'] = (isset($_POST['remise'])?$_POST['remise']:null);


$erreurs = ['designation' => "", 'pu' => "",'unitecond' => "", 'remise' => ""];


$refart = $_SESSION['edit'];

$reqUnArt = "SELECT * FROM article WHERE refart = $refart";
$resultat = $db->query($reqUnArt);
$resulta = $resultat->fetch();

$valeurs['refart'] = $resulta['refart'];
$valeurs['designation'] = $resulta['designation'];
$valeurs['pu'] = $resulta['pu'];
$valeurs['unitecond']= $resulta['unitecond'];
$valeurs['remise'] = $resulta['remise'];

unset($resulta);


if ((isset($_POST['valider']))) {
  
    $erreurs = erreursDonnees($valeurs);
   
    if (donneesOK($erreurs)) {
      
        $modif['designation'] = (isset($_POST['designation']) ? $_POST['designation'] : null);
        $modif['pu'] = (isset($_POST['pu']) ? $_POST['pu'] : null);
        $modif['unitecond']= (isset($_POST['unitecond']) ? $_POST['unitecond'] : null);
        $modif['remise'] = (isset($_POST['remise']) ? $_POST['remise'] : null);

        $erreurs = erreursDonnees($modif);

        if (donneesOK($erreurs)) {
            $result = $db->prepare($reqUpdateArt);
            $result->execute(
                [
                    ':refart' => $valeurs['refart'],
                    ':designation' => $modif['designation'],
                    ':pu' => $modif['pu'],
                    ':unitecond' => $modif['unitecond'],
                    ':remise' => $modif['remise']
                ]
            );
            unset($result);
            header('location: ./admin.php');
        }
    }
}

if(isset($_POST['retour'])){
    header ('Location: ./admin.php');
}


include 'modif.view.php';
?>