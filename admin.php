<?php
session_start();

class Article
{
    private $refart;
    private $Designation;
    private $Prixunit;
    private $Unitcond;
    private $Remise;




    function __construct($refart, $Designation, $Prixunit, $Unitcond, $Remise)
    {
        $this->refart = $refart;
        $this->Designation = $Designation;
        $this->Prixunit = $Prixunit;
        $this->Unitcond = $Unitcond;
        $this->Remise = $Remise;
    }


    function getRefart()
    {
        return $this->refart;
    }

    function getDesignation()
    {
        return $this->Designation;
    }

    function getPrixunit()
    {
        return $this->Prixunit;
    }

    function getUnitcond()
    {
        return $this->Unitcond;
    }

    function getRemise()
    {
        return $this->Remise;
    }

    function calculPrixRemise()
    {
        return $this->Prixunit - ($this->Prixunit * $this->Remise / 100);
    }

    function __toString()
    {
        return "Référence : " . $this->refart . " Designation : " . $this->Designation;
    }

    function __affichePrix()
    {
        return "Prix :" . $this->Prixunit . "€ <br/> Remise : " . $this->Remise . "<br/> Prix remisé : " . $this->calculPrixRemise() . "€";
    }

}
function afficheClients()
{
    include 'tobdd.php';
    include 'Requete.php';
    $result = $db->prepare($listeclient);
    $result->execute();
    echo "<form action='admin.php' method='post'>";
    echo '<table>
    <tr>
    <th>Id</th>
    <th>Nom</th>
    <th>Prenom</th>
    </tr> ';
    foreach ($result as $ligne) {
        echo '<tr><td>' . $ligne['idutilisateur'] . '</td>' .
            '<td>' . $ligne['nom'] . '</td>' .
            '<td>' . $ligne['prenom'] . '</td>';
        echo '<td><button name="modifier" value=' . $ligne['idutilisateur'] . '>Modifier</button></td>' . '
    <td><button name="supprimer" value=' . $ligne['idutilisateur'] . '>Supprimer</button></td>';
        echo '</tr>';
    }
    echo '</table>';
    echo '</br><button name="ajout">Ajouter un nouveau client</button>';
    echo '</form>';
    unset($result);
}


function afficheart()
{
    include 'tobdd.php';
    include 'Requete.php';
    $result = $db->prepare($listeart);
    $result->execute();
    echo "<form action='admin.php' method='post'>";
    echo '<table>
    <tr>
    <th>Reference</th>
    <th>Designation</th>
    <th>Prix unite</th>
    <th>Unite conditionnement</th>
    <th>Remise</th>
    </tr> ';
    foreach ($result as $ligne) {
        echo '<tr><td>' . $ligne['refart'] . '</td>' .
            '<td>' . $ligne['designation'] . '</td>' .
            '<td>' . $ligne['pu'] . '</td>' .
            '<td>' . $ligne['unitecond'] . '</td>' .
            '<td>' . $ligne['remise'] . '</td>';
        echo '<td><button name="modifierart" value=' . $ligne['refart'] . '>Modifier</button></td>' . '
    <td><button name="supprimerart" value=' . $ligne['refart'] . '>Supprimer</button></td>';
        echo '</tr>';
    }
    echo '</table>';
    echo '</br><button name="ajoutart">Ajouter un nouvel article</button>';
    echo '</form>';
    unset($result);
}

if (isset($_POST['importer'])) {
    $target_file = "/" . basename($_FILES["fil"]["name"]);
    if (move_uploaded_file($_FILES["fil"]["tmp_name"], $target_file)) {
    } 
    $strJsonFileContents = file_get_contents($_FILES["fil"]["name"]);
    $array = json_decode($strJsonFileContents, true);
    include 'tobdd.php';
    include 'Requete.php';

    $arts = [];
    $result = $db->prepare($listeart);
    $result->execute();
    foreach ($result as $art) {
        $arts[] = new Article($art['refart'], $art['designation'], $art['pu'], $art['unitecond'], $art['remise']);
    }
    $ajout = $array["ajout"];
    $modif = $array["modif"];
    $suppr = $array["suppr"];

    function existeArt($refart){
        include ('tobdd.php');
        include ('Requete.php');
        $existe = false;
        $requete = "SELECT * FROM article WHERE refart = '$refart'";
        $resultat = $db->query($requete);
        $compte = $resultat->fetch();
        if ($resultat->rowCount() == 1) {
            $existe = true;
        }
        return $existe;
    }

    if (isset($ajout)) {
        
        foreach ($ajout as $ligne) {
            if (existeArt($ligne['ref']) == false) {
                $result = $db->prepare($ajoutarticle2);
                $result->execute(
                array(
                    ':refart' => $ligne['ref'],
                    ':designation' => $ligne['desi'],
                    ':pu' => $ligne['pu'],
                    ':unitecond' => $ligne['unite'],
                )
            );
            }
        }
    }

    if (isset($modif)) {
        
        foreach ($modif as $ligne) {
            $desi = null;
            $pu = null;
            $unite = null;
            $remise = null;
            $ref = $ligne['ref'];


            if (!empty($ligne['desi'])) {
                $desi = $ligne['desi'];
            }

            if (!empty($ligne['pu'])) {
                $pu = $ligne['pu'];
            }

            if (!empty($ligne['unite'])) {
                $unite = $ligne['unite'];
            }

            if (!empty($ligne['remise'])) {
                $remise = $ligne['remise'];
            }



            foreach ($arts as $art) {
                if ($art->getRefart() == $ref) {
                    if ($desi == null) {
                        $desi = $art->getDesignation();
                    }

                    if ($pu == null) {
                        $pu = $art->getPrixunit();
                    }

                    if ($unite == null) {
                        $unite = $art->getUnitcond();
                    }

                    if ($remise == null) {
                        $remise = $art->getRemise();
                    }
                }
            }

            $result = $db->prepare($reqUpdateArt2);
            $result->execute(
                array(
                    ':refart' => $ref,
                    ':designation' => $desi,
                    ':pu' => $pu,
                    ':unitecond' => $unite,
                    ':remise' => $remise,
                )
            );
        }

        if (isset($suppr)) {
            foreach ($suppr as $ligne) {
                $result = $db->prepare($supprart);
                $result->execute(
                    array(
                        ':refart' => $ligne['ref'],
                    )
                );
            }
        }
    }
}

if (isset($_POST['ajoutart'])) {
    header('Location: ./ajoutart.php');
}

if (isset($_POST['modifierart'])) {
    $_SESSION['edit'] = $_POST['modifierart'];
    header('Location: ./modif.php');
}

if (isset($_POST['supprimerart'])) {
    include 'tobdd.php';
    $refart = $_POST['supprimerart'];
    $requete = "DELETE FROM caddie WHERE refart = '$refart'";
    $resultat = $db->query($requete);
    $requete = "DELETE FROM article WHERE refart = '$refart'";
    $resultat = $db->query($requete);
    header('Location: ./admin.php');
}

if (isset($_POST['ajout'])) {
    $_SESSION['edit'] = 1;
    header('Location: ./Inscription.php');
}

if (isset($_POST['modifier'])) {
    $_SESSION['edit'] = $_POST['modifier'];
    header('Location: ./compte.php');
}

if (isset($_POST['supprimer'])) {
    include 'tobdd.php';
    $id = $_POST['supprimer'];
    $requete = "DELETE FROM caddie WHERE idutilisateur = '$id'";
    $resultat = $db->query($requete);
    $requete = "DELETE FROM utilisateur WHERE idutilisateur = '$id'";
    $resultat = $db->query($requete);
    header('Location: ./admin.php');
}

include 'admin.view.php';

?>