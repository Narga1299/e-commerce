<html>
<head>
    <meta charset="UTF-8" />
    <link rel="stylesheet" href="inscription.css">
</head>
<header class="fnoir">
        <h1>Boutique en ligne de l'IUT de Metz</h1>
</header>
<body>
    <h2>Inscription</h2>
    <div class="box">
    <form  method = 'post' action = 'Inscription.php' name='form1'>
        <div>
            <h3>Formulaire de saisie</h3>
            <div>
                <label>Nom :</label>
                <input type="text" name="Nom" value = "<?=$nom?>" placeholder="Indiquez votre nom" />
            </div>
            <span class="erreur"><?php echo $erreurs['nom']; ?></span>
            <div>
                <label>Prénom :</label>
                <input type="text" name="Prenom" value = '<?=$prenom?>' placeholder="Indiquez votre prénom"/>
            </div>
            <span class="erreur"><?php echo $erreurs['prenom']; ?></span>
            <div>
                <label> Genre : </label>
                <input type="radio" name="Civilite"  value = "Mme"
                                <?php echo ($civil  == "Mme")?'checked = "checked"': "" ?>/>Féminin 
                <input type="radio" name="Civilite"  value = "M."            
                                <?php  if ($civil === "M." || $civil ===null)  
                                echo 'checked = "checked;"'?> />Masculin
            </div>
            <div>
                <label>Adresse mail :</label>
                <input type="text" name="Mail" value = '<?=$mel?>' placeholder="Indiquez votre adresse mail"/>
            </div>
            <span class="erreur"><?php echo $erreurs['mel']; ?></span>
        </div>
        <div>
            <div>
                <label>Login :</label>
                <input type="text" name="Logi" value = '<?=$login?>' placeholder="Indiquez votre identifiant"/>
            </div>
            <span class="erreur"><?php echo $erreurs['login']; ?></span>
        </div>
        <div>
            <div>
                <label>Mot de passe :</label>
                <input type="text" name="MDP" value = '<?=$mdp?>' placeholder="Indiquez votre mot de passe "/>
            </div>
            <span class="erreur"><?php echo $erreurs['mdp']; ?></span>
        </div>
        <div class="bu">
            <input  type="submit" name="valider" value="Validez">
            <input  type="submit" name="Annuler" value="Annulez">
            <input  type="submit" id="retour" name= "retour" value="Retour" />
            </form>
        </div>
</body>
</html>