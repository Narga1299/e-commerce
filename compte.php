<?php
session_start();

function affichecompte()
{
    include 'tobdd.php';
    $iduti = $_SESSION['id'];
    if ($_SESSION['edit']!==null){
    $iduti = $_SESSION['edit'];
    }
    $requete = "SELECT * FROM utilisateur WHERE idutilisateur = '$iduti'";
    $resultat = $db->query($requete);
    $compte = $resultat->fetch();
    echo "Nom : " . $compte['nom'] . "</br>";
    echo "</br>";
    echo "Prénom : " . $compte['prenom'] . "</br>";
    echo "</br>";
    echo "Civilité : ";
    if ($compte['civilite'] == "M.") {
        echo "Monsieur";
    } else {
        echo "Madame";
    }
    echo "<br>";
    echo "</br>";
    echo "Email : " . $compte['mel'] . "<br>";
    echo "</br>";
    echo "Login : " . $compte['login'] . "<br>";
    echo "</br>";
    if ($_SESSION['edit']!==null){
        echo 'Status : ';
        if ($compte['admin'] == 1) {
            echo "Administrateur";
        } else {
            echo "Client";
        }
    }
    else{
        echo "</br>";
        echo "Mot de passe : " . $_SESSION['mdp'] . "<br>";
    }
    echo "<br>";
    echo '<p class="boutonmodif"><a href = "compte2.php">Modifier</a></p>';
}

function erreursDonnees(array $donnees,$admin)
{
    if ($admin!=null){
    $erreurs = ['prenom' => "", 'mel' => ""];
    }
    else{
    $erreurs = ['prenom' => "", 'mel' => "", 'mdp' => ""];
    }

    if (!isset($donnees['prenom']) or empty($donnees['prenom']))
        $erreurs['prenom'] = "saisie obligatoire du prenom";
    
    if (!isset($donnees['mel']) or empty($donnees['mel']))
        $erreurs['mel'] = "saisie obligatoire de l'email";
    
        
    if ($admin==null){
    if (!isset($donnees['mdp']) or empty($donnees['mdp']))
        $erreurs['mdp'] = "saisie obligatoire du mot de passe";
    }

    return $erreurs;
}



function donneesOK(array $erreurs)
{
    $nbErreurs = 0;
    foreach ($erreurs as $erreur) {
        if ($erreur != "")
            $nbErreurs++;
    }
    echo $nbErreurs;
    return ($nbErreurs == 0);
}


include 'compte.view.php';
?>


