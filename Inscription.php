<?php
session_start();
include ('tobdd.php');
include ('Requete.php');
$valeurs['civilite'] = "";

$nom = (isset($_POST['Nom'])?$_POST['Nom']:null);
$prenom = (isset($_POST['Prenom'])?$_POST['Prenom']:null);
$mel = (isset($_POST['Mail'])?$_POST['Mail']:null);
$civil = (isset($_POST['Civilite'])?$_POST['Civilite']:null);
$login = (isset($_POST['Logi'])?$_POST['Logi']:null);
$mdp = (isset($_POST['MDP'])?$_POST['MDP']:null);


$erreurs = ['nom'=>"", 'prenom'=>"", 'mel'=>"",'login' => "", 'mdp' => ""];

if (isset($_POST['valider'])){
    if (!isset($nom) or empty($nom)){
        $erreurs['nom'] = "Veuillez saisir votre nom";
    }
    if (!isset($prenom) or empty($prenom)){
        $erreurs['prenom'] = "Veuillez saisir votre prénom";
    }
    if (!isset($mel) or empty($mel)){
        $erreurs['mel'] = "Veuillez saisir votre adresse mail";
    }
    if (!isset($login) or empty($login)){
        $erreurs['login'] = "Veuillez saisir votre identifiant";
    }
    if (!isset($mdp) or empty($mdp)){
        $erreurs['mdp'] = "Veuillez saisir votre mot de passe";
    }

    $nbErreurs = 0;
    foreach ($erreurs as $erreur){
    if ($erreur != "") $nbErreurs++;
    }

    if ($nbErreurs==0){
    $mdphash = password_hash($mdp, PASSWORD_DEFAULT); 
    $req = "INSERT INTO utilisateur (nom, prenom, civilite,mel,login,mdp,admin) 
            VALUES (:nom, :prenom, :civilite , :mel, :login, :mdp,0)";
    $insert = $db->prepare($req);
    $insert -> execute(array( ':nom' => $nom, 
    ':prenom' => $prenom, 
    ':civilite' => $civil, 
    ':mel' => $mel, 
    ':login' => $login, 
    ':mdp' => $mdphash));
    if ($_SESSION['edit'] == 1){
        header('Location: ./admin.php');
    }
    else{
        header('Location: ./accueil.php');
    }
}
unset($insert );
    $nom = "";
    $prenom = "";
    $mel = "";
    $login = "";
    $mdp = "";
}  

if(isset($_POST['retour'])){
    if($_SESSION['edit'] !== null){
        header('Location: ./admin.php');
    }
    else{
        header('Location: ./accueil.php');
    }
}



include 'Inscription.view.php';
?>