<!doctype html>
 <html>
 <head>
        <meta charset='SET NAMES utf8'/>
        <link rel="stylesheet" href="modif.css">
    <title>Modif article</title>
</head>
<header class="fnoir">
        <h1>Boutique en ligne de l'IUT de Metz</h1>
</header>
<body>
<h2>
                Modification article
            </h2>
    <section class="box">
        <article>
            
            <form method="POST" action="" name="modif">
                <table class="formulaire">
                    <tr>
                        <td><label for="refart">Reference : </label></td>
                        <td><input type="text" name="refart" id="refart" size="30" value="<?= $valeurs['refart'] ?>" disabled  /></td>
                    </tr>
                    <tr>
                        <td><label for="designation">Designation: </label></td>
                        <td><input type="text" name="designation" id="designation" size="30" value="<?= $valeurs['designation'] ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><span class="erreur">
                                <?php echo $erreurs['designation']; ?>
                            </span></td>
                    </tr>
                    <tr>
                        <td><label for="mel">Prix unitaire : </label></td>
                        <td><input type="number" name="pu" id="pu" size="30" step="0.01" value="<?= $valeurs['pu'] ?>" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><span class="erreur">
                                <?php echo $erreurs['pu']; ?>
                            </span></td>
                    </tr>
                    <tr>
                        <td><label for="unitecond">Unite de conditionnement : </label></td>
                        <td><input type="text" name="unitecond" id="unitecond" size="30" value="<?= $valeurs['unitecond'] ?>" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><span class="erreur">
                                <?php echo $erreurs['unitecond']; ?>
                            </span></td>
                    </tr>
                    <tr>
                        <td><label for="remise">Remise : </label></td>
                        <td><input type="text" name="remise" id="remise"  size="30" value="<?= $valeurs['remise'] ?>" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><span class="erreur">
                                <?php echo $erreurs['remise']; ?>
                            </span></td>
                    </tr>
                </table>
                <div class="bu">
                    <input type="submit" id="valider" name= "valider" value="Valider" />
                    <input type="submit" id="retour" name= "retour" value="Retour" />
                </div>
            </form>
        </article>
    </section>
</body>

</html>

